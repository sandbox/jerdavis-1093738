Drupal.behaviors.giftaid = function(context) {
  $('form.pay-giftaid').click(function() {
    if ($('#edit-giftaid-giftaid:checked').val() == 1) {
      var giftaidAmount;
      var giftaidRate;
      if ($('.donate-recommended-amount:checked').val() > 1) {
        $giftaidAmount = $('.donate-recommended-amount:checked').val();
      }
      else if ($('.donate-other-amount').val() > 0) {
        $giftaidAmount = $('.donate-other-amount').val();
      }
      else {
        $giftaidAmount = 0;
      }
      $giftaidRate = $('#edit-giftaid-giftaid-rate').val();
      $giftaidAmount = ($giftaidAmount * $giftaidRate).toFixed(2);
      if ($giftaidAmount > 0) {
        $('form .giftaid-amount').html($giftaidAmount);
      }
      else {
        $('form .giftaid-amount').html('');
      }
    }
    else {
      $('form .giftaid-amount').html('');
    }
  }).trigger('click');
}
