<?php

function giftaid_views_data() {
  $data = array();

  $data['giftaid_transaction']['table']['group']  = t('Gift Aid transactions');

  $data['giftaid_transaction']['pxid'] = array(
    'title' => t('Transaction ID'),
    'help' => t('The unique identifier for a payment transaction.'),
    'relationship' => array(
      'base' => 'pay_transaction',
      'field' => 'pxid',
      'base field' => 'pxid',
      'label' => t('Gift Aid transaction'),
      'type' => 'LEFT',
    ),
  );

  $data['giftaid_transaction']['table']['join'] = array(
    'pay_transaction' => array(
      'left_field' => 'pxid',
      'field' => 'pxid',
      'type' => 'LEFT',
    ),
  );

  $data['giftaid_transaction']['status'] = array(
    'title' => t('Gift Aid status'),
    'help' => t('Whether or not the transaction should be submitted to Gift Aid.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Published'),
      'type' => 'yes-no',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  return $data;
}