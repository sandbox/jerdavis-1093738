<?php

class giftaid extends donate {
  var $giftaid_donation = FALSE;
  var $giftaid_rate = 0;
  var $giftaid_lead;

  function settings_form(&$form, &$form_state) {
    parent::settings_form($form, $form_state);
    $group = $this->handler();
    $form[$group]['donate']['giftaid_rate'] = array(
      '#type' => 'textfield',
      '#size' => 20,
      '#title' => t('Gift Aid rate'),
      '#description' => t('Define the default rate to use for Gift Aid donation suggestions.'),
      '#default_value' => $this->giftaid_rate,
    );
    $form[$group]['donate']['giftaid_lead'] = array(
      '#type' => 'textfield',
      '#size' => 80,
      '#title' => t('Gift Aid lead-in'),
      '#description' => t('Define the lead-in text for Gift Aid donation suggestions.'),
      '#default_value' => $this->giftaid_lead,
    );
  }

  function form(&$form, &$form_state) {
    // Allow the parent class to define the form.
    parent::form($form, $form_state);

    // Add JS for giftaid calculation and behaviors.
    drupal_add_js(drupal_get_path('module','giftaid') .'/js/giftaid.js');

    // Allow the parent to define the handler for form processing.
    $group = $this->handler();

    $form[$group]['amount']['#weight'] = -12;
    $form[$group]['amount_other']['#weight'] = -11;
    $form[$group]['amount_display']['#weight'] = -11;

    $form[$group]['giftaid'] = array(
      '#type' => 'checkbox',
      '#title' => t('Gift Aid'),
      '#description' => t('Make a Gift Aid donation.'),
      '#default_value' => $this->giftaid_donation,
      '#weight' => -10,
      '#prefix' => '<div class="giftaid-wrapper">',
    );
    $form[$group]['giftaid_amount'] = array(
      '#type' => 'markup',
      '#value' => '<span class="giftaid-lead">' . $this->giftaid_lead . '<span><span class="giftaid-amount"></span>',
      '#weight' => -9,
      '#suffix' => '</div>',
    );

    // Hidden fields to carry settings values through.
    $form[$group]['giftaid_lead'] = array(
      '#type' => 'hidden',
      '#default_value' => $this->giftaid_lead,
    );
    $form[$group]['giftaid_rate'] = array(
      '#type' => 'hidden',
      '#default_value' => $this->giftaid_rate,
    );

    // There's a confirmation message.  Display the message instead of the form.
    if (isset($form_state['storage']['confirmation'])) {
      $form[$group] = array(
        '#type' => 'markup',
        '#value' => check_markup($this->confirmation_text, $this->confirmation_format),
      );

      // Remove submit button on confirmation page.
      $form['submit']['#access'] = FALSE;
    }
  }

  function form_submit($form, &$form_state) {
    parent::form_submit($form, $form_state);
    if ($form_state['values']['giftaid']['giftaid'] === 1) {
      $record = array(
        'pxid' => $this->transaction->pxid,
        'status' => $form_state['values']['giftaid']['giftaid'],
      );
      drupal_write_record('giftaid_transaction', $record);
    }
  }
}